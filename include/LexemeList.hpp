#ifndef ENGINE_LEXEME_LIST_HPP
#define ENGINE_LEXEME_LIST_HPP

// #include <Lexeme.hpp>
#include <vector>
#include <Lexeme.hpp>
#include <LexemeAnalizeResult.hpp>
#include <Statement.hpp>

namespace bls
{

LexemeAnalizeResult lexemeListAnalizeStatement(
	const std::vector<Lexeme> &list,
	const Lexeme &summonerLexeme,
	const Statement &statement,
	const std::string &lexemeName
);

std::vector<Lexeme> lexemeListGetPossibleLexemes(
	const std::vector<Lexeme> &list,
	const Statement &statement
);

}

#endif