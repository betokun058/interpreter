#ifndef ENGINE_TOKEN_DEFINTION_HPP
#define ENGINE_TOKEN_DEFINTION_HPP

#include <regex>
#include <Token.hpp>

namespace bls
{

/// Struct used to define tokens based on regular expressions
struct TokenDefinition
{
	/// The type of the token
	Token::Type type;

	/// The value that represents non-custom tokens
	std::string value;

	/// Holds the custom type of the token
	std::string customType;

	/// Used to identify the token
	std::regex regEx;

	/// Constructor for non-custom tokens
	TokenDefinition(
		Token::Type type,
		const std::string &value
	);

	/// Constructor for custom tokens, regEx must be escaped correctly
	TokenDefinition(
		const std::string &customType = "",
		const std::string &regEx = ""
	);
};

}

#endif