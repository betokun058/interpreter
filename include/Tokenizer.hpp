#ifndef ENGINE_TOKENIZER_HPP
#define ENGINE_TOKENIZER_HPP

#include <vector>
#include <Token.hpp>
#include <TokenDefinition.hpp>
#include <TokenizerAnalizeResult.hpp>
#include <TokenizerReadResult.hpp>

namespace bls
{

class Interpreter;

/**
* Responsible for identifying every single token inside the language based
* on defined delimiters, operators, keywords, string delimietrs,
* escape characters, and user-defined tokens
*/
class Tokenizer
{
public:
	/// Possible states of the Tokenizer
	enum class State
	{
		Reading,
		ReadingOperator,
		ReadingString
	};

	/// Tokenizer classifies every character with these values
	enum class CharacterType
	{
		NoCharacter = 0,
		Normal,
		Delimiter,
		Operator,
		StringDelimiter,
		Escape
	};

	static char NoStringDelimiter;

	/// Default constructor
	Tokenizer();

	/**
	*  Resets the state of the tokenizer, this way it's ready to
	* interpret new text
	*/
	void reset();

	/// Sets the interpreter the tokenizer belongs to
	void setInterpreter(Interpreter *interpreter);

	/**
	* Analize the given character and taking into account isLastCharacter,
	* current state, and already-read characters, identifies tokens
	*/
	TokenizerAnalizeResult analizeCharacter(
		char character,
		bool isLastCharacter
	);

private:
	/**
	* The current state of the tokenizer. It can be modified by read(),
	* readOperators(), and readString(). Starts on State::Reading
	*/
	State state;

	/// The previous character type. Starts on CharacterType::NoCharacter
	CharacterType previousCharacterType;

	/// Holds the character that openned the current-reading string
	char opennerStringDelimiter;

	/**
	* A possible token inside of the code. Characters are added to this by
	* read(), readOperator(), and readString(). It's clear when the token
	* is generated in generateToken()
	*/
	std::string tokenValue;

	/// A pointer to the interpreter this belongs to
	Interpreter *interpreter;

	/**
	* Based on the given string and the registered keywords, operators, and
	* custom delimiters, generate tokens. It does not recognize string
	* tokens
	*/
	Token generateToken(const std::string &value) const;

	/// Determines the type of the given character
	CharacterType getCharacterType(char character) const;

	/**
	* Called when the state is State::Reading. If the given character is
	* CharacterType::Normal holds the state, in case of
	* CharacterType::Delimiter instructs to validate the token; if it's
	* CharacterType::Operator the token is validated the state is
	* changed to State::ReadingOperator, and the characters is not consumed,
	* the same if it's CharacterType::StringDelimiter but the state is
	* changed to State::ReadingString
	*/
	TokenizerReadResult read(CharacterType characterType, char character);

	/**
	* Called when the state is State::ReadingOperator. If the character
	* is CharacterType::Normal, CharacterType::Delimiter, o
	* CharacterType::StringDelimiter the token is validated, the character
	* is consume in both CharacterType::Normal and CharacterType::Delimiter,
	* and the state is changed to State::Reading in both
	* State::Normal, and State::Delimiter, and to State::ReadingString in
	* case of CharacterType::StringDelimiter
	*/
	TokenizerReadResult readOperator(
		CharacterType characterType,
		char character
	);

	/**
	* Called when the state is State::ReadingString. Only if the
	* character is CharacterType::StringDelimiter and if it's the closer
	* string delimiter, instructs to generate a string token, and the
	* state is changed to State::Reading. The character is added to
	* the token value otherwise
	*/
	TokenizerReadResult readString(
		CharacterType characterType,
		char character
	);

	/**
	* Executes the action determined by read(), readOperator(), and
	* readString(). Generates string tokens if required, or call
	* generateToken() in case that the current character is the last one
	* or requred. In both cases, clears the value of the token
	*/
	Token executeReadResult(
		TokenizerReadResult::Type readResultType,
		bool isLastCharacter
	);

	/**
	* Based on the given string and the registered operators, return
	* the count of operators the string could be
	*/
	int getPossibleOperators(const std::string &str) const;
};

}

#endif