#ifndef ENGINE_STATEMENT_HPP
#define ENGINE_STATEMENT_HPP

#include <Lexeme.hpp>
#include <Token.hpp>

namespace bls
{

class Statement
{
public:
	void addToken(const Token &token);
	void addSubstatement(const Statement &substatement);
	void clear();
	const std::vector <Token> & getTokens() const;
	void setSubstatements(const std::vector <Statement> &substatements);
	const std::vector <Statement> & getSubstatements() const;

	Lexeme lexeme;

private:
	std::vector <Token> tokens;
	std::vector <Statement> substatements;
};

}

#endif