#ifndef ENGINE_INTERPRETER_HPP
#define ENGINE_INTERPRETER_HPP

#include <string>
#include <vector>
#include <Lexeme.hpp>
#include <Parser.hpp>
#include <Statement.hpp>
#include <Tokenizer.hpp>
#include <TokenDefinition.hpp>

namespace bls
{
class Interpreter
{
public:
	Interpreter();

	void addTokenDefinition(Token::Type type, const std::string &value);
	void addTokenDefinition(
		const std::string &customType,
		const std::string &regEx
	);
	std::vector<TokenDefinition> getTokenDefinitions(
		Token::Type type = Token::Type::Invalid
	) const;

	void addLexeme(const std::string &lexeme, const std::string &type);
	void setMultiOptionalLexemeOperator(const std::string &opeartor_);
	const std::vector<Lexeme> & getLexemes() const;

	void interpret(const std::string &text);
	void interpretFile(const std::string &filePath);

private:
	std::vector<TokenDefinition> tokenDefinitions;
	std::vector<Lexeme> lexemes;
	std::string multiOptionalLexemeOperator;
	std::vector<Statement> statements;

	Tokenizer tokenizer;
	Parser parser;

	void removeTokenDefinition(Token::Type type, const std::string &value);
};

}

#endif