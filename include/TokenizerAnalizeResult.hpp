#ifndef ENGINE_TOKENIZER_ANALIZE_RESULT_HPP
#define ENGINE_TOKENIZER_ANALIZE_RESULT_HPP

#include <Token.hpp>

namespace bls
{

/**
* Holds the result of Tokenizer::analizeCharacter() as well as the
* action the Interpreter must execute, consume or not consume the
* current character
*/
struct TokenizerAnalizeResult
{
	/// Type of action the interpreter must execute
	enum class Type
	{
		Consume,
		NoConsume
	};

	/// The action the interpreter must execute
	Type type;

	/// The generated token
	Token token;
};

}

#endif