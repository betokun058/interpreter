#ifndef ENGINE_GAME_INTERPRETER_HPP
#define ENGINE_GAME_INTERPRETER_HPP

#include <Interpreter.hpp>

namespace bls
{

//////////////////////////////
//////////////////////////////
class GameInterpreter :
	public Interpreter
{
public:
	//////////////////////////////
	//////////////////////////////
	GameInterpreter();
};

}

#endif