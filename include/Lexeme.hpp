#ifndef ENGINE_LEXEME_HPP
#define ENGINE_LEXEME_HPP

#include <string>
#include <vector>
#include <Token.hpp>

namespace bls
{

class Statement;
struct LexemeAnalizeResult;

struct Lexeme
{
	enum class InstanceType
	{
		PossibleInstance,
		Instance,
		NoInstance
	};

	std::string name;
	std::vector<Token> tokens;
	std::vector<Lexeme> nestedLexemes;

	Lexeme();
	Lexeme(const std::string &name,
		const std::vector<Token> &tokens,
		const std::vector<Lexeme> &nestedLexemes);
	LexemeAnalizeResult analizeStatement(
		const Statement &statement
	) const;
};

}

#endif