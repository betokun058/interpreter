#ifndef ENGINE_PARSER_HPP
#define ENGINE_PARSER_HPP

#include <Lexeme.hpp>
#include <LexemeList.hpp>
#include <ParserAnalizeResult.hpp>
#include <Statement.hpp>
#include <Token.hpp>

namespace bls
{

class Interpreter;

class Parser
{
public:
	Parser();

	void setInterpreter(Interpreter *interpreter);
	void reset();

	ParserAnalizeResult analizeToken(const Token &token);
	std::vector<Lexeme> getPossibleLexemes(
		const Statement &statement,
		const std::vector<Lexeme> &lexemes
	) const;

private:
	Interpreter *interpreter;
	Statement currentStatement;
	std::vector<Lexeme> possibleLexemes;
};

}

#endif