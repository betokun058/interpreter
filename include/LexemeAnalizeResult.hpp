#ifndef ENGINE_LEXEME_ANALIZE_RESULT_HPP
#define ENGINE_LEXEME_ANALIZE_RESULT_HPP

#include <Statement.hpp>

namespace bls
{

struct LexemeAnalizeResult
{
	enum class Type
	{
		PossibleInstance,
		Instance,
		NoInstance
	};

	Type type;
	Statement statement;

	LexemeAnalizeResult(
		Type type,
		const Statement &statement = Statement()
	);
};

}

#endif