#ifndef ENGINE_TOKEN_HPP
#define ENGINE_TOKEN_HPP

#include <string>

namespace bls
{

/**
* Holds info related to the tokens of the language (i.e., operators,
* keywords, strings, etc)
*/
struct Token
{
	/// Token types
	enum class Type
	{
		Invalid,
		Unknown,
		Keyword,
		Operator,
		String,
		Custom,
		Escape,
		Delimiter,
		Lexeme,
		MultiOptionalLexeme,
		MultiLexeme,
		OptionalLexeme
	};

	/// Token type
	Type type;

	/// Token value
	std::string value;

	/**
	* String that indicates the token type, possible values are the type in
	* lowercase or a user-defined value for Type::Custom
	*/
	std::string customType;

	/// Default constructor
	Token(
		Type type = Type::Unknown,
		const std::string &value = "",
		const std::string &customType = "");

	bool operator==(const Token &other) const;
	bool operator!=(const Token &other) const;

	bool isLexeme() const;
	bool isOptional() const;
	bool isMultiple() const;

	std::string getLexemeName() const;
};

}

#endif