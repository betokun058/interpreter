#ifndef ENGINE_TOKENIZER_READ_RESULT_HPP
#define ENGINE_TOKENIZER_READ_RESULT_HPP

namespace bls
{

/**
* Holds the result of Tokenizer::read(), Tokenizer::readOperator(), and
* Tokenizer::readString(), the action the Tokenizer must execute
* as well as the action the Interpreter must execute
*/
struct TokenizerReadResult
{
	/// Type of action the Tokenizer must execute
	enum class Type
	{
		KeepReading,
		ValidateToken,
		GenerateStringToken
	};

	/// The action the Tokenizer must execute
	Type type;

	/// The action the Interpreter must execute
	TokenizerAnalizeResult::Type analizeResultType;
};

}

#endif