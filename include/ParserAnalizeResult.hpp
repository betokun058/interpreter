#ifndef ENGINE_PARSER_ANALIZE_RESULT_HPP
#define ENGINE_PARSER_ANALIZE_RESULT_HPP

#include <Statement.hpp>

namespace bls
{

struct ParserAnalizeResult
{
	enum class Type
	{
		KeepReading,
		Statement,
		UnexpectedToken
	};

	Type type;
	Statement statement;

	ParserAnalizeResult(Type type);
};

}

#endif