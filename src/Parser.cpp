#include <Parser.hpp>
#include <Interpreter.hpp>
#include <LexemeAnalizeResult.hpp>

namespace bls
{
Parser::Parser() :
	interpreter(nullptr)
{
}

void Parser::setInterpreter(Interpreter *interpreter)
{
	this->interpreter = interpreter;
}

void Parser::reset()
{
	this->possibleLexemes = std::vector<Lexeme>();
	this->currentStatement.clear();
}

ParserAnalizeResult Parser::analizeToken(const Token &token)
{
	ParserAnalizeResult result(ParserAnalizeResult::Type::UnexpectedToken);
	this->currentStatement.addToken(token);

	const auto &possibleLexemes = this->getPossibleLexemes(
		this->currentStatement,
		this->possibleLexemes);

	if (possibleLexemes.size() > 1)
	{
		this->possibleLexemes = possibleLexemes;
		result.type = ParserAnalizeResult::Type::KeepReading;
	}
	else if (possibleLexemes.size() == 1)
	{
		const auto &analizeResult = possibleLexemes[0]
			.analizeStatement(this->currentStatement);

		if (analizeResult.type == LexemeAnalizeResult::Type::Instance)
		{
			result.type = ParserAnalizeResult::Type::Statement;
			result.statement = analizeResult.statement;
			this->currentStatement.clear();
			this->possibleLexemes.clear();
		}
		else
		{
			this->possibleLexemes = possibleLexemes;
			result.type = ParserAnalizeResult::Type::KeepReading;
		}
	}
	else
	{
		this->currentStatement.clear();
		this->possibleLexemes.clear();
	}

	return result;
}

std::vector<Lexeme> Parser::getPossibleLexemes(
	const Statement &statement,
	const std::vector<Lexeme> &lexemes
) const
{
	const auto &lexemesToFilter =
		lexemes.empty() && this->interpreter ?
			this->interpreter->getLexemes() :
			lexemes;

	return lexemeListGetPossibleLexemes(lexemesToFilter, statement);
}

}
