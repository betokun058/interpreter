#include <Lexeme.hpp>
#include <LexemeAnalizeResult.hpp>
#include <LexemeList.hpp>
#include <Statement.hpp>
#include <iostream>

namespace bls
{
Lexeme::Lexeme()
{
}

//////////////////////////////
Lexeme::Lexeme(
	const std::string &name,
	const std::vector<Token> &tokens,
	const std::vector<Lexeme> &nestedLexemes
) :
	name(name),
	tokens(tokens),
	nestedLexemes(nestedLexemes)
{
}

LexemeAnalizeResult Lexeme::analizeStatement(
	const Statement &statement
) const
{
	auto tokenIt = this->tokens.begin();
	const auto &statementTokens = statement.getTokens();
	auto statementTokenIt = statementTokens.begin();
	Statement tempStatement;
	Statement substatement;

	while (tokenIt != this->tokens.end() &&
		statementTokenIt != statementTokens.end())
	{
		if (!tokenIt->isLexeme())
		{
			if (*tokenIt != *statementTokenIt)
				return LexemeAnalizeResult(
					LexemeAnalizeResult::Type::NoInstance);


			tempStatement.addToken(*statementTokenIt);
			++tokenIt;
			++statementTokenIt;
		}
		else
		{
			substatement.addToken(*statementTokenIt);
			const auto result = lexemeListAnalizeStatement(
				this->nestedLexemes,
				*this,
				substatement,
				tokenIt->getLexemeName());

			if (result.type == LexemeAnalizeResult::Type::PossibleInstance)
			{
				++statementTokenIt;
			}

			if (result.type == LexemeAnalizeResult::Type::Instance)
			{
				tempStatement.addSubstatement(tempStatement);
				substatement.clear();
				++statementTokenIt;

				if (!tokenIt->isMultiple())
				{
					++tokenIt;
				}
			}

			if (result.type == LexemeAnalizeResult::Type::NoInstance)
			{
				if (tokenIt->isOptional())
				{
					substatement.clear();
					++tokenIt;
				}
				else
					return LexemeAnalizeResult(
						LexemeAnalizeResult::Type::NoInstance);
			}
		}
	}

	if (tokenIt == this->tokens.end() &&
		statementTokenIt == statementTokens.end())
	{
		tempStatement.lexeme = *this;
		return LexemeAnalizeResult(
			LexemeAnalizeResult::Type::Instance,
			tempStatement);
	}

	if (statementTokenIt == statementTokens.end())
	{
		return LexemeAnalizeResult(
			LexemeAnalizeResult::Type::PossibleInstance);
	}

	return LexemeAnalizeResult(LexemeAnalizeResult::Type::NoInstance);
}

}