#include <TokenDefinition.hpp>

namespace bls
{
//////////////////////////////
TokenDefinition::TokenDefinition(
	Token::Type type,
	const std::string &value
) :
	type(type),
	value(value)
{
	switch (type)
	{
		case Token::Type::Keyword: this->customType = "keyword"; break;
		case Token::Type::String: this->customType = "string"; break;
		case Token::Type::Operator: this->customType = "operator"; break;
		case Token::Type::Escape: this->customType = "escape"; break;
	}
}

//////////////////////////////
TokenDefinition::TokenDefinition(
	const std::string &customType,
	const std::string &regEx
) :
	type(Token::Type::Custom),
	customType(customType),
	value(regEx)
{
	this->regEx.assign(regEx);
}

}