#include <LexemeList.hpp>

namespace bls
{
LexemeAnalizeResult lexemeListAnalizeStatement(
	const std::vector<Lexeme> &list,
	const Lexeme &summonerLexeme,
	const Statement &statement,
	const std::string &lexemeName
)
{
	for (const auto &lexeme : list)
	{
		if (lexeme.name == lexemeName)
		{
			const auto &result = lexeme.analizeStatement(statement);
			if (result.type != LexemeAnalizeResult::Type::NoInstance)
				return result;
		}
	}

	if (summonerLexeme.name == lexemeName)
	{
		const auto &result = summonerLexeme.analizeStatement(statement);
		if (result.type != LexemeAnalizeResult::Type::NoInstance)
			return result;
	}

	return LexemeAnalizeResult(LexemeAnalizeResult::Type::NoInstance);
}

std::vector<Lexeme> lexemeListGetPossibleLexemes(
	const std::vector<Lexeme> &list,
	const Statement &statement
)
{
	std::vector<Lexeme> possibleLexemes;

	for (const auto &lexeme : list)
	{
		if (lexeme.analizeStatement(statement).type !=
				LexemeAnalizeResult::Type::NoInstance)
		{
			possibleLexemes.emplace_back(lexeme);
		}
	}

	return possibleLexemes;
}

}