#include <Token.hpp>

namespace bls
{
//////////////////////////////
Token::Token(
	Token::Type type,
	const std::string &value /* = "" */,
	const std::string &customType /* = "" */) :
	type(type),
	value(value),
	customType(customType)
{
}

bool Token::operator==(const Token &other) const
{
	if (this->type != other.type)
	{
		return false;
	}

	if (this->type == Type::Custom)
	{
		return this->customType == other.customType;
	}

	if (this->type == Type::String)
	{
		return true;
	}

	return this->value == other.value;
}

bool Token::operator!=(const Token &other) const
{
	return !(*this == other);
}

bool Token::isLexeme() const
{
	return this->type == Type::Lexeme ||
		this->type == Type::MultiLexeme ||
		this->type == Type::OptionalLexeme ||
		this->type == Type::MultiOptionalLexeme;
}

bool Token::isOptional() const
{
	return this->type == Type::OptionalLexeme ||
		this->type == Type::MultiOptionalLexeme;
}

bool Token::isMultiple() const
{
	return this->type == Type::MultiLexeme ||
		this->type == Type::MultiOptionalLexeme;
}

std::string Token::getLexemeName() const
{
	if (!this->isLexeme())
		return "";

	return this->value.substr(this->value.find_first_of(')') + 1);
}

}