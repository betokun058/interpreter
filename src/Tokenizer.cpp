#include <Tokenizer.hpp>
#include <Interpreter.hpp>
#include <TokenizerReadResult.hpp>
#include <regex>

namespace bls
{
//////////////////////////////
char Tokenizer::NoStringDelimiter = 0;

//////////////////////////////
Tokenizer::Tokenizer() :
	state(Tokenizer::State::Reading),
	previousCharacterType(Tokenizer::CharacterType::NoCharacter),
	opennerStringDelimiter(Tokenizer::NoStringDelimiter),
	interpreter(nullptr)
{
}

//////////////////////////////
void Tokenizer::reset()
{
	this->state = Tokenizer::State::Reading;
	this->previousCharacterType = Tokenizer::CharacterType::NoCharacter;
}

//////////////////////////////
void Tokenizer::setInterpreter(Interpreter *interpreter)
{
	this->interpreter = interpreter;
}

//////////////////////////////
TokenizerAnalizeResult Tokenizer::analizeCharacter(
	char character,
	bool isLastCharacter
)
{
	auto characterType = this->getCharacterType(character);
	TokenizerReadResult readResult;

	switch (this->state)
	{
		case State::Reading:
			readResult = this->read(characterType, character);
		break;

		case State::ReadingOperator:
			readResult = this->readOperator(characterType, character);
		break;

		case State::ReadingString:
			readResult = this->readString(characterType, character);
		break;
	}

	auto token = this->executeReadResult(readResult.type, isLastCharacter);

	if (readResult.analizeResultType == TokenizerAnalizeResult::Type::Consume)
	{
		this->previousCharacterType = characterType;
	}

	return {
		.type = readResult.analizeResultType,
		.token = token
	};
}

//////////////////////////////
TokenizerReadResult Tokenizer::read(
	CharacterType characterType,
	char character
)
{
	switch (characterType)
	{
		case CharacterType::Normal:
			this->tokenValue += character;
			return {
				.type = TokenizerReadResult::Type::KeepReading,
				.analizeResultType = TokenizerAnalizeResult::Type::Consume
			};

		case CharacterType::Delimiter:
			return {
				.type = !this->tokenValue.empty() ?
					TokenizerReadResult::Type::ValidateToken :
					TokenizerReadResult::Type::KeepReading,
				.analizeResultType = TokenizerAnalizeResult::Type::Consume
			};

		case CharacterType::Operator:
			this->state = State::ReadingOperator;
			return {
				.type = !this->tokenValue.empty() ?
					TokenizerReadResult::Type::ValidateToken :
					TokenizerReadResult::Type::KeepReading,
				.analizeResultType = TokenizerAnalizeResult::Type::NoConsume
			};

		case CharacterType::StringDelimiter:
			this->state = State::ReadingString;
			return {
				.type = !this->tokenValue.empty() ?
					TokenizerReadResult::Type::ValidateToken :
					TokenizerReadResult::Type::KeepReading,
				.analizeResultType = TokenizerAnalizeResult::Type::NoConsume
			};
	}
}

//////////////////////////////
TokenizerReadResult Tokenizer::readOperator(
	CharacterType characterType,
	char character
)
{
	switch (characterType)
	{
		case CharacterType::Operator:
			// Could be a composed operator
			if (this->previousCharacterType == CharacterType::Operator)
			{
				int possibleOperators = this->getPossibleOperators(
					this->tokenValue + character);

				// Normal operator
				if (possibleOperators == 0)
				{
					this->state = State::Reading;
					return {
						.type = TokenizerReadResult::Type::ValidateToken,
						.analizeResultType =
							TokenizerAnalizeResult::Type::NoConsume
					};
				}
				else // Composed operator
				{
					this->tokenValue += character;
					return {
						.type = TokenizerReadResult::Type::KeepReading,
						.analizeResultType = TokenizerAnalizeResult::Type::Consume
					};
				}
			}
			else // Could be the begin of a composed operator
			{
				this->tokenValue += character;

				auto possibleOperators = this->getPossibleOperators(
					this->tokenValue);

				// Normal operator
				TokenizerReadResult::Type readResultType;
				if (possibleOperators == 1)
				{
					readResultType = TokenizerReadResult::Type::ValidateToken;
					this->state = State::Reading;
				}
				else
				{
					readResultType = TokenizerReadResult::Type::KeepReading;
				}

				return {
					.type = readResultType,
					.analizeResultType = TokenizerAnalizeResult::Type::Consume
				};
			}

		case CharacterType::Delimiter:
			this->state = State::Reading;
			return {
				.type = TokenizerReadResult::Type::ValidateToken,
				.analizeResultType = TokenizerAnalizeResult::Type::Consume
			};

		case CharacterType::Normal:
			this->state = State::Reading;
			return {
				.type = TokenizerReadResult::Type::ValidateToken,
				.analizeResultType = TokenizerAnalizeResult::Type::NoConsume
			};

		case CharacterType::StringDelimiter:
			this->state = State::ReadingString;
			return {
				.type = TokenizerReadResult::Type::ValidateToken,
				.analizeResultType = TokenizerAnalizeResult::Type::NoConsume
			};
	}
}

//////////////////////////////
TokenizerReadResult Tokenizer::readString(
	CharacterType characterType,
	char character
)
{
	switch (characterType)
	{
		case CharacterType::StringDelimiter:
			// Consume the initial string-openner character
			if (this->tokenValue.empty() &&
				this->opennerStringDelimiter == NoStringDelimiter)
			{
				this->opennerStringDelimiter = character;
				return {
					.type = TokenizerReadResult::Type::KeepReading,
					.analizeResultType = TokenizerAnalizeResult::Type::Consume
				};
			}

			// is the same and not escaped, the end of the string
			if (character == this->opennerStringDelimiter &&
				this->previousCharacterType != CharacterType::Escape)
			{
				this->state = State::Reading;
				this->opennerStringDelimiter = NoStringDelimiter;
				return {
					.type = TokenizerReadResult::Type::GenerateStringToken,
					.analizeResultType = TokenizerAnalizeResult::Type::Consume
				};
			}
			// Escaped character
			else
			{
				this->tokenValue.pop_back();
				this->tokenValue += character;

				return {
					.type = TokenizerReadResult::Type::KeepReading,
					.analizeResultType = TokenizerAnalizeResult::Type::Consume
				};
			}
		break;

		default:
			this->tokenValue += character;
			return {
				.type = TokenizerReadResult::Type::KeepReading,
				.analizeResultType = TokenizerAnalizeResult::Type::Consume
			};
	}
}

//////////////////////////////
Token Tokenizer::executeReadResult(
	TokenizerReadResult::Type readResultType,
	bool isLastCharacter
)
{
	Token token(Token::Type::Invalid);

	if (readResultType == TokenizerReadResult::Type::GenerateStringToken)
	{
		token = Token(Token::Type::String,
			this->tokenValue,
			"string");
		this->tokenValue.clear();
	}
	else if (readResultType == TokenizerReadResult::Type::ValidateToken ||
		isLastCharacter)
	{
		token = this->generateToken(this->tokenValue);
		this->tokenValue.clear();
	}

	return token;
}

//////////////////////////////
Token Tokenizer::generateToken(const std::string &value) const
{
	Token token(Token::Type::Unknown, value);

	// Test if the token is a keyword
	for (const auto &tokenDefinition :
		this->interpreter->getTokenDefinitions(Token::Type::Keyword))
	{
		if (tokenDefinition.value == token.value)
		{
			token.type = Token::Type::Keyword;
			token.customType = tokenDefinition.customType;
			return token;
		}
	}

	// Test if the token is a operator
	for (const auto &tokenDefinition :
		this->interpreter->getTokenDefinitions(Token::Type::Operator))
	{
		if (tokenDefinition.value == token.value)
		{
			token.type = Token::Type::Operator;
			token.customType = tokenDefinition.customType;
			return token;
		}
	}

	// Test if the token match any token definition
	std::smatch match;

	for (const auto &tokenDefinition :
		this->interpreter->getTokenDefinitions(Token::Type::Custom))
	{
		if (std::regex_match(token.value, match, tokenDefinition.regEx))
		{
			token.type = Token::Type::Custom;
			token.customType = tokenDefinition.customType;
			break;
		}
	}

	return token;
}

//////////////////////////////
Tokenizer::CharacterType Tokenizer::getCharacterType(char character) const
{
	if (!this->interpreter)
	{
		return CharacterType::NoCharacter;
	}

	// Is it delimiter?
	for (const auto &tokenDefinition :
		this->interpreter->getTokenDefinitions(Token::Type::Delimiter))
	{
		if (character == tokenDefinition.value[0])
		{
			return CharacterType::Delimiter;
		}
	}

	// Is it escape?
	for (const auto &tokenDefinition :
		this->interpreter->getTokenDefinitions(Token::Type::Escape))
	{
		if (character == tokenDefinition.value[0])
		{
			return CharacterType::Escape;
		}
	}

	// Is it operator?
	if (this->getPossibleOperators(std::string(1, character)) > 0)
	{
		return CharacterType::Operator;
	}

	// Is it string delimiter?
	for (const auto &tokenDefinition :
		this->interpreter->getTokenDefinitions(Token::Type::String))
	{
		if (character == tokenDefinition.value[0])
		{
			return CharacterType::StringDelimiter;
		}
	}

	return CharacterType::Normal;
}

//////////////////////////////
int Tokenizer::getPossibleOperators(const std::string &str) const
{
	int counter = 0;

	for (const auto &tokenDefinition :
		this->interpreter->getTokenDefinitions(Token::Type::Operator))
	{
		if (tokenDefinition.value.find(str) != std::string::npos)
		{
			if (tokenDefinition.value == str)
			{
				counter++;
			}
			// This way, we force to keep reading
			else
			{
				counter += 2;
			}
		}
	}

	return counter;
}


}