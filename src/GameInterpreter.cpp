#include <GameInterpreter.hpp>

namespace bls
{
//////////////////////////////
GameInterpreter::GameInterpreter()
{
	// Delimiters
	this->addTokenDefinition(Token::Type::Delimiter, " ");
	this->addTokenDefinition(Token::Type::Delimiter, "\n");
	this->addTokenDefinition(Token::Type::Delimiter, "\t");

	// Keywords
	this->addTokenDefinition(Token::Type::Keyword, "def");
	this->addTokenDefinition(Token::Type::Keyword, "include");

	// Operators
	this->addTokenDefinition(Token::Type::Operator, "=");
	this->addTokenDefinition(Token::Type::Operator, "<");
	this->addTokenDefinition(Token::Type::Operator, "</");
	this->addTokenDefinition(Token::Type::Operator, ">");
	this->addTokenDefinition(Token::Type::Operator, "/>");

	// Strings
	this->addTokenDefinition(Token::Type::String, "'");
	this->addTokenDefinition(Token::Type::Escape, "\\");

	// Custom tokens
	this->addTokenDefinition("identifier", "\\w+");

	////// Lexemes

	this->addLexeme("include ''", "include_file");

	// Variable definition
	this->addLexeme("def variable_name", "variable_definition");
	this->addLexeme("def variable_name = ''", "variable_definition");

	// Entity definition
	this->addLexeme("param_name=''", "entity_param");
	this->addLexeme("<entity_name entity_param.[]? />", "entity_definition");
	this->addLexeme(
		"<entity_name entity_param.[]?>entity_definition.[]?</entity_name>",
		"entity_definition");
}

}