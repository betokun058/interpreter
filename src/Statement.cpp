#include <Statement.hpp>

namespace bls
{
void Statement::addToken(const Token &token)
{
	this->tokens.push_back(token);
}
void Statement::addSubstatement(const Statement &substatement)
{
	this->substatements.push_back(substatement);
}

void Statement::clear()
{
	this->tokens = std::vector<Token>();
	this->substatements = std::vector<Statement>();
}

const std::vector <Token> & Statement::getTokens() const
{
	return this->tokens;
}

void Statement::setSubstatements(const std::vector <Statement> &substatements)
{
	this->substatements = substatements;
}

const std::vector <Statement> & Statement::getSubstatements() const
{
	return this->substatements;
}

}