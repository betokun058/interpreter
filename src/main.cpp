#include <GameInterpreter.hpp>
#include <iostream>

int main(int argc, char **argv)
{
	if (argc == 1)
	{
		std::cout << "No input files" << std::endl;
		return 0;
	}

	bls::GameInterpreter interpreter;
	for (int i = 1; i < argc; ++i)
	{
		interpreter.interpretFile(argv[i]);
	}

	return 0;
}