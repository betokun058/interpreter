#include <LexemeAnalizeResult.hpp>
#include <Statement.hpp>

namespace bls
{
LexemeAnalizeResult::LexemeAnalizeResult(
	LexemeAnalizeResult::Type type,
	const Statement &statement
) :
	type(type),
	statement(statement)
{
}
}