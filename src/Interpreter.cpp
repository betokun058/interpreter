#include <Interpreter.hpp>
#include <iostream>
#include <fstream>

namespace bls
{
//////////////////////////////
Interpreter::Interpreter() :
	multiOptionalLexemeOperator(".[]?")
{
	this->tokenizer.setInterpreter(this);
	this->parser.setInterpreter(this);
}

//////////////////////////////
void Interpreter::addTokenDefinition(
	Token::Type type,
	const std::string &value
)
{
	this->tokenDefinitions.emplace_back(type, value);
}

//////////////////////////////
void Interpreter::addTokenDefinition(
	const std::string &customType,
	const std::string &regEx
)
{
	this->tokenDefinitions.emplace_back(customType, regEx);
}

//////////////////////////////
std::vector<TokenDefinition> Interpreter::getTokenDefinitions(
	Token::Type type
) const
{
	if (type == Token::Type::Invalid)
		return this->tokenDefinitions;

	std::vector<TokenDefinition> filteredTokenDefinitions;

	for (const auto &tokenDefinition : this->tokenDefinitions)
	{
		if (tokenDefinition.type == type)
		{
			filteredTokenDefinitions.push_back(tokenDefinition);
		}
	}

	return filteredTokenDefinitions;
}

//////////////////////////////
void Interpreter::removeTokenDefinition(
	Token::Type type,
	const std::string &value)
{
	for (auto tokenDefinition = this->tokenDefinitions.begin();
		tokenDefinition != this->tokenDefinitions.end();
		++tokenDefinition)
	{
		if (tokenDefinition->type == type &&
			tokenDefinition->value == value)
		{
			this->tokenDefinitions.erase(tokenDefinition);
			return;
		}
	}
}

//////////////////////////////
void Interpreter::addLexeme(
	const std::string &lexeme,
	const std::string &lexemeName
)
{
	this->addTokenDefinition(
		Token::Type::Operator,
		this->multiOptionalLexemeOperator);

	std::vector<Token> tokens;
	std::vector<Lexeme> nestedLexemes;

	for (auto i = 0U; i < lexeme.size();)
	{
		char character = lexeme[i];
		auto analizeResult = this->tokenizer.analizeCharacter(
			character,
			i == lexeme.size() - 1);
		auto token = analizeResult.token;

		switch (token.type)
		{
			case Token::Type::Custom:
				// Let's test if it's a lexeme
				for (const auto &possibleLexeme : this->lexemes)
				{
					if (possibleLexeme.name == token.value)
					{
						nestedLexemes.push_back(possibleLexeme);
						token.type = Token::Type::Lexeme;
						token.customType = "lexeme";
						token.value.insert(0, "(lex)");
					}
				}

				tokens.push_back(token);
			break;

			case Token::Type::Operator:
				// Multi-optional token
				if (token.value == this->multiOptionalLexemeOperator)
				{
					auto &lastToken = tokens.back();

					if (lastToken.type == Token::Type::Lexeme)
					{
						lastToken.type = Token::Type::MultiOptionalLexeme;
						lastToken.customType = "multi-optional-lexeme";
						lastToken.value.replace(0, 5, "(multi-opt-lex)");
					}
				}
				else
				{
					tokens.push_back(token);
				}
			break;

			case Token::Type::Unknown:
				this->tokenizer.reset();
				std::cout << "Error registering lexeme, unknow token "
					<< token.value << std::endl;
				return;

			case Token::Type::Invalid: break;

			default:
				tokens.push_back(token);
			break;
		}

		if (analizeResult.type == TokenizerAnalizeResult::Type::Consume)
		{
			++i;
		}
	}

	std::cout << "Registering lexeme (" << lexemeName << ") (" <<
		nestedLexemes.size() << "): ";
	for (const auto &token : tokens)
	{
		std::cout << token.customType << " ";
	}
	std::cout << std::endl;
	this->lexemes.emplace_back(lexemeName, tokens, nestedLexemes);

	this->removeTokenDefinition(
		Token::Type::Operator,
		this->multiOptionalLexemeOperator);
	this->tokenizer.reset();
}

//////////////////////////////
void Interpreter::setMultiOptionalLexemeOperator(const std::string &operator_)
{
	this->multiOptionalLexemeOperator = operator_;

}

//////////////////////////////
const std::vector<Lexeme> & Interpreter::getLexemes() const
{
	return this->lexemes;
}

//////////////////////////////
void Interpreter::interpret(const std::string &text)
{
	for (auto i = 0U; i < text.size();)
	{
		char character = text[i];
		const auto &tokenizerResult = this->tokenizer.analizeCharacter(
			character,
			i == text.size() - 1);
		const auto &token = tokenizerResult.token;

		if (token.type == Token::Type::Unknown)
		{
			std::cout << "Unknown token: " << token.value << std::endl;
			return;
		}
		else if (token.type != Token::Type::Invalid)
		{
			// std::cout << "Token: (" << token.customType
			// 	<< ") " << token.value << std::endl;
			const auto &parserResult = this->parser.analizeToken(token);

			if (parserResult.type ==
				ParserAnalizeResult::Type::UnexpectedToken)
			{
				std::cout << "Unexpected token: " << token.customType
					<< std::endl;
				return;
			}
			else if (parserResult.type ==
				ParserAnalizeResult::Type::Statement)
			{
				const auto &statement = parserResult.statement;
				std::cout << "Statement (" << statement.lexeme.name << ") "
					<< "(" << statement.getSubstatements().size() << ") ";
				for (const auto &token : statement.getTokens())
				{
					std::cout << token.value << " ";
				}
				std::cout << std::endl;
				this->parser.reset();
			}
		}

		if (tokenizerResult.type == TokenizerAnalizeResult::Type::Consume)
		{
			++i;
		}
	}

	this->tokenizer.reset();
	this->parser.reset();
}

//////////////////////////////
void Interpreter::interpretFile(const std::string &filePath)
{
	std::ifstream file(filePath);

	if (!file.is_open())
	{
		std::cout << "Unable to open " << filePath << std::endl;
		return;
	}

	std::string fileContent;
	while (file)
	{
		char character = file.get();
		if (!file.eof())
		{
			fileContent += character;
		}
	}

	this->interpret(fileContent);
}

}